#!/usr/bin/env zsh

if [[ -z "$DOTFILES_PATH" ]]; then
  echo 'DOTFILES_PATH is not set, run install.sh to start'
  return
fi

source "$DOTFILES_PATH/system/.alias.sh"
source "$DOTFILES_PATH/system/.env.sh"
source "$DOTFILES_PATH/system/.function.sh"
source "$DOTFILES_PATH/system/.prompt.sh"
source "$DOTFILES_PATH/system/zsh/.zsh_completion.sh"
source "$DOTFILES_PATH/system/zsh/.zsh_prompt.sh"
source "$DOTFILES_PATH/system/zsh/.zsh_extra.sh"

for custom_sh in $(find "$DOTFILES_PATH/custom" -type f -name "*.sh"); do
  source "$custom_sh";
done
