" enable syntax highlighting
syntax on

" VCS will take responsibility for these
set nobackup
set nowritebackup
set noswapfile
