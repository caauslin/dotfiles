# Lin's Dotfiles

Personal dotfiles and system hacks, mainly for macOS and Ubuntu.

## Resources

* https://dotfiles.github.io/
* https://medium.com/@webprolific/getting-started-with-dotfiles-43c3602fd789
* https://github.com/robbyrussell/oh-my-zsh
* https://github.com/mathiasbynens/dotfiles
