#!/usr/bin/env zsh

# do not beep on errors
unsetopt beep

# save command history
HISTFILE="$HOME/.zsh_history"
HISTSIZE=1000
SAVEHIST=1000

setopt append_history
setopt extended_history # write the history file in the ":start:elapsed;command" format
setopt hist_expire_dups_first # expire duplicate entries first when trimming history
setopt hist_ignore_dups # ignore duplication command history list
setopt hist_ignore_space # don't record an entry starting with a space
setopt hist_verify # don't execute immediately upon history expansion
setopt inc_append_history # write to the history file immediately, not when the shell exits
setopt share_history # share command history data
