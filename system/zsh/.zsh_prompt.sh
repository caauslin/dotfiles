autoload -Uz vcs_info
autoload -Uz colors && colors

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*' formats "%{$reset_color%}:%{$fg[green]%}%b%{$reset_color%}"

precmd() {
  vcs_info
}

setopt prompt_subst
PROMPT='%F{cyan}[%c${vcs_info_msg_0_}%F{cyan}]%f $ '

# fix url not correctly parsed in CLI arguments
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic
