#!/usr/bin/env zsh

# make completion case in-sensitive
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'

# enable advance auto-complete and arrow-select interface
autoload -Uz compinit && compinit
zstyle ':completion:*' menu select

# auto quote pasted URL
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic

autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic
