if [[ "$OSTYPE" == "darwin"* ]]; then
  # resume CWD in new terminal tab, see /etc/bashrc_Apple_Terminal
  if [ -z "$INSIDE_EMACS" ]; then
    update_terminal_cwd() {
      # Identify the directory using a "file:" scheme URL, including
      # the host name to disambiguate local vs. remote paths.

      # Percent-encode the pathname.
      local url_path=''
      {
        # Use LC_CTYPE=C to process text byte-by-byte. Ensure that
        # LC_ALL isn't set, so it doesn't interfere.
        local i ch hexch LC_CTYPE=C LC_ALL=
        for ((i = 1; i <= ${#PWD}; ++i)); do
          ch="${PWD[i,i]}"
          if [[ "$ch" =~ [/._~A-Za-z0-9-] ]]; then
            url_path+="$ch"
          else
            printf -v hexch "%02X" "'$ch"
            # printf treats values greater than 127 as
            # negative and pads with "FF", so truncate.
            url_path+="%${hexch: -2:2}"
          fi
        done
      }

      printf '\e]7;%s\a' "file://$HOSTNAME$url_path"
    }
    precmd_functions+=(update_terminal_cwd)
  fi
fi
