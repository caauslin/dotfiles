# when using sudo, use alias expansion (otherwise sudo ignores your aliases)
alias sudo='sudo '

# ls commands
if [[ "$OSTYPE" == "darwin"* ]]; then
  alias l='ls -AG'
  alias la='ls -AG'
  alias ll='ls -AGlh'
else
  alias l='ls -A --color'
  alias la='ls -A --color'
  alias ll='ls -Alh --color'
fi

# directory navigation
alias -- -='cd -'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# just make life easier for frequent windows users
alias cls='clear'

# colorize grep and exclude VCS
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
