# add custom executables
export PATH="$DOTFILES_PATH/bin:$PATH"

# add system sepcific custom executables
if [[ "$OSTYPE" == "darwin"* ]]; then
  export PATH="$DOTFILES_PATH/bin/darwin:$PATH"
fi
