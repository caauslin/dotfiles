#!/usr/bin/env bash

set -euo pipefail

declare FORCE_YES=

prompt_yn() {
  if [ -n "$FORCE_YES" ]; then
    return 0
  fi

  while true; do
    read -r -n 1 -p "$1 [y/n] " yn
    case $yn in
      [Yy]) echo; return 0; break ;;
      [Nn]) echo; return 1; break ;;
      *)
    esac
  done
}

install::macos() {
  if prompt_yn "[MACOS] Change default screenshots folder to '$HOME/Pictures/Screenshots'?"; then
    defaults write com.apple.screencapture location "$HOME/Pictures/Screenshots"
    echo 'Default screenshots folder updated'
  fi

  if [ -d "$HOME/Library/Rime" ]; then
    if prompt_yn "[MACOS] Override RIME configurations?"; then
      ln -si "$(pwd)/rc/rime/default.custom.yaml" "$HOME/Library/Rime/default.custom.yaml"
      ln -si "$(pwd)/rc/rime/squirrel.custom.yaml" "$HOME/Library/Rime/squirrel.custom.yaml"
      echo 'RIME configurations updated'
    fi
  fi
}

# install::linux() {
# }

install() {
  if [[ "${1-}" =~ ^(-y|--yes)$ ]]; then
    FORCE_YES=1
  fi

  if ! prompt_yn 'Install dotfiles?'; then
    echo 'Cancelled'
    return
  fi

  if [ -z "${DOTFILES_PATH:-}" ]; then
    export DOTFILES_PATH="$(pwd)"
    printf "\nexport DOTFILES_PATH=\"%s\"\nsource \"\$DOTFILES_PATH/rc/.zshrc\"\n" "$DOTFILES_PATH" >> "$HOME/.zshrc"
    echo ".zshrc updated"
  else
    echo "DOTFILES_PATH is already set, skip .zshrc setup"
  fi

  if [ ! -e "$HOME/.vimrc" ]; then
    ln -s "$(pwd)/rc/.vimrc" "$HOME/.vimrc"
    echo '.vimrc linked'
  fi

  case "$OSTYPE" in
    darwin*)
      install::macos "$@"
      ;;
    linux*)
      # install::linux "$@"
      ;;
  esac
}

install "$@"
